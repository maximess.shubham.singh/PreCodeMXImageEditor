//
//  MXCanvasView.swift
//  PreCodeMXImageEditor
//
//  Created by Shubham Singh on 21/04/23.
//

import UIKit

class MXCanvasView: UIView {
    
    
    private let scrollView: UIScrollView = {
        let o = UIScrollView()
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    private let scrollContentView: UIView = {
        let o = UIView()
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    private let imageView: UIImageView = {
        let o = UIImageView()
        o.contentMode = .scaleAspectFit
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.image = nil
        setupViews()
        configureScrollView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    var image: UIImage? {
        set {
            self.imageView.image = newValue
            self.changeZoomScale(for: .scaleAspectFit)
        }
        get {
            return self.imageView.image
        }
    }
    
    var zoomScale: CGFloat {
        set {
            self.scrollView.zoomScale = newValue
        }
        get {
            return self.scrollView.zoomScale
        }
    }
    
    var offset: CGPoint {
        set {
            self.scrollView.contentOffset = newValue
        }
        get {
            return self.scrollView.contentOffset
        }
    }
    
    override var contentMode: UIView.ContentMode {
        set {
            self.changeZoomScale(for: newValue)
        }
        get {
            return self.scrollView.zoomScale > 1 ? .scaleAspectFill : .scaleAspectFit
        }
    }
    
    private func setupViews() {
        
        backgroundColor = .systemBackground
        
        addSubview(scrollView)
        addConstraints([
            NSLayoutConstraint(item: scrollView, attribute: .top, relatedBy: .equal, toItem: self, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollView, attribute: .bottom, relatedBy: .equal, toItem: self, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollView, attribute: .leading, relatedBy: .equal, toItem: self, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollView, attribute: .trailing, relatedBy: .equal, toItem: self, attribute: .trailing, multiplier: 1, constant: 0),
        ])
        
        scrollView.addSubview(scrollContentView)
        scrollView.addConstraints([
            NSLayoutConstraint(item: scrollContentView, attribute: .top, relatedBy: .equal, toItem: scrollView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollContentView, attribute: .bottom, relatedBy: .equal, toItem: scrollView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollContentView, attribute: .leading, relatedBy: .equal, toItem: scrollView, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollContentView, attribute: .trailing, relatedBy: .equal, toItem: scrollView, attribute: .trailing, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollContentView, attribute: .width, relatedBy: .equal, toItem: scrollView, attribute: .width, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: scrollContentView, attribute: .height, relatedBy: .equal, toItem: scrollView, attribute: .height, multiplier: 1, constant: 0),
        ])
        
        scrollContentView.addSubview(imageView)
        scrollContentView.addConstraints([
            NSLayoutConstraint(item: imageView, attribute: .top, relatedBy: .equal, toItem: scrollContentView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: imageView, attribute: .bottom, relatedBy: .equal, toItem: scrollContentView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: imageView, attribute: .leading, relatedBy: .equal, toItem: scrollContentView, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: imageView, attribute: .trailing, relatedBy: .equal, toItem: scrollContentView, attribute: .trailing, multiplier: 1, constant: 0),
        ])
    }
    
    func changeZoomScale(for contentMode: UIView.ContentMode) {
        guard let image = self.image else { return }
        let imageSize = image.size
        switch contentMode {
        case .scaleAspectFit:
            self.scrollView.zoomScale = 1
        case .scaleAspectFill:
            let canvasHeightToWidthRatio = self.bounds.height / self.bounds.width
            let imageHeightToWidthRatio = imageSize.height / imageSize.width
            let zoomScaleH = imageHeightToWidthRatio / canvasHeightToWidthRatio
            
            let canvasWidthToHeightRatio = self.bounds.width / self.bounds.height
            let imageWidthToHeightRatio = imageSize.width / imageSize.height
            let zoomScaleW = imageWidthToHeightRatio / canvasWidthToHeightRatio
            
            let zoomScale = zoomScaleH > zoomScaleW ? zoomScaleH : zoomScaleW
            self.scrollView.zoomScale = zoomScale
        default:
            self.scrollView.zoomScale = 1
        }
    }
    
    func configureScrollView() {
        scrollView.delegate = self
        scrollView.maximumZoomScale = 5
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(doubleTapScrollView))
        tapGesture.numberOfTapsRequired = 2
        scrollView.addGestureRecognizer(tapGesture)
    }
    
    @objc func doubleTapScrollView(_ sender: UITapGestureRecognizer) {
        if scrollView.zoomScale == 1 {
            scrollView.setZoomScale(4, animated: true)
        } else {
            scrollView.setZoomScale(1, animated: true)
        }
    }
}

extension MXCanvasView: UIScrollViewDelegate {
    func viewForZooming(in scrollView: UIScrollView) -> UIView? {
        return scrollContentView
    }
    
    func scrollViewDidZoom(_ scrollView: UIScrollView) {
        //print(scrollView.zoomScale)
        print(scrollView.contentOffset)
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        imageView.clipsToBounds = false
        scrollView.clipsToBounds = true
    }
    
    func scrollViewWillBeginZooming(_ scrollView: UIScrollView, with view: UIView?) {
        imageView.clipsToBounds = false
        scrollView.clipsToBounds = true
    }
    
    func scrollViewDidEndZooming(_ scrollView: UIScrollView, with view: UIView?, atScale scale: CGFloat) {
        imageView.clipsToBounds = true
        scrollView.clipsToBounds = true
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        imageView.clipsToBounds = true
        scrollView.clipsToBounds = true
    }
}
