//
//  ViewController.swift
//  PreCodeMXImageEditor
//
//  Created by Shubham Singh on 21/04/23.
//

import UIKit

class ViewController: UIViewController {

    let sampleImagesTableView: UITableView = {
        let o = UITableView()
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    var sampleImages: [UIImage] = [
        .portraitSampleImage1,
        .portraitSampleImage2,
        .portraitSampleImage3,
        .extraPortraitSampleImage1,
        .extraPortraitSampleImage2,
        .extraPortraitSampleImage3,
        .landscapeSampleImage1,
        .landscapeSampleImage2,
        .landscapeSampleImage3,
        .extraLandscapeSampleImage1,
        .extraLandscapeSampleImage2,
        .squareSampleImage1,
        .squareSampleImage2
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        sampleImagesTableView.delegate = self
        sampleImagesTableView.dataSource = self
        sampleImagesTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        setupViews()
    }

    private func setupViews() {
        view.addSubview(sampleImagesTableView)
        let item = sampleImagesTableView
        let toItem = view.safeAreaLayoutGuide
        view.addConstraints([
            NSLayoutConstraint(item: item, attribute: .top, relatedBy: .equal, toItem: toItem, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: item, attribute: .bottom, relatedBy: .equal, toItem: toItem, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: item, attribute: .leading, relatedBy: .equal, toItem: toItem, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: item, attribute: .trailing, relatedBy: .equal, toItem: toItem, attribute: .trailing, multiplier: 1, constant: 0),
        ])
    }

}

extension ViewController: UITableViewDataSource, UITableViewDelegate {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sampleImages.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath)
        cell.imageView?.image = sampleImages[indexPath.row]
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let canvasViewController = MXCanvasViewController(image: sampleImages[indexPath.row])
        canvasViewController.modalPresentationStyle = .fullScreen
        self.present(canvasViewController, animated: true)
    }
}

class SampleImageCell: UITableViewCell {
    
    static let reuseId = "SampleImageCell"
    
}
