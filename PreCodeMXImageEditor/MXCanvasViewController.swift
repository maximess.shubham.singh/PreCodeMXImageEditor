//
//  MXCanvasViewController.swift
//  PreCodeMXImageEditor
//
//  Created by Shubham Singh on 21/04/23.
//

import UIKit

class MXCanvasViewController: UIViewController {
    
    var image: UIImage?
    var previousOffset: CGPoint?
    
    init(image: UIImage) {
        super.init(nibName: nil, bundle: nil)
        self.image = image
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private let canvasView: MXCanvasView = {
        let o = MXCanvasView()
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = .systemBackground
        setupViews()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.canvasView.image = self.image
    }
    
    private func setupViews() {
        view.addSubview(self.canvasView)
        let item = self.canvasView
        let toItem = view.safeAreaLayoutGuide
        view.addConstraints([
            NSLayoutConstraint(item: item, attribute: .top, relatedBy: .equal, toItem: toItem, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: item, attribute: .bottom, relatedBy: .equal, toItem: toItem, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: item, attribute: .leading, relatedBy: .equal, toItem: toItem, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: item, attribute: .trailing, relatedBy: .equal, toItem: toItem, attribute: .trailing, multiplier: 1, constant: 0),
        ])
    }
    
}
