//
//  AppDelegate.swift
//  PreCodeMXImageEditor
//
//  Created by Shubham Singh on 21/04/23.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {



    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

extension UIImage {
    static let portraitSampleImage1 = UIImage(named: "Portrait-SampleImage1")!
    static let portraitSampleImage2 = UIImage(named: "Portrait-SampleImage2")!
    static let portraitSampleImage3 = UIImage(named: "Portrait-SampleImage3")!
    static let portraitSampleImage4 = UIImage(named: "Portrait-SampleImage4")!
    static let portraitSampleImage5 = UIImage(named: "Portrait-SampleImage5")!
    static let portraitSampleImage6 = UIImage(named: "Portrait-SampleImage6")!
    
    static let extraPortraitSampleImage1 = UIImage(named: "ExtraPortrait-SampleImage1")!
    static let extraPortraitSampleImage2 = UIImage(named: "ExtraPortrait-SampleImage2")!
    static let extraPortraitSampleImage3 = UIImage(named: "ExtraPortrait-SampleImage3")!
    
    static let landscapeSampleImage1 = UIImage(named: "Landscape-SampleImage1")!
    static let landscapeSampleImage2 = UIImage(named: "Landscape-SampleImage2")!
    static let landscapeSampleImage3 = UIImage(named: "Landscape-SampleImage3")!
    
    static let extraLandscapeSampleImage1 = UIImage(named: "ExtraLandscape-SampleImage1")!
    static let extraLandscapeSampleImage2 = UIImage(named: "ExtraLandscape-SampleImage2")!
    
    static let squareSampleImage1 = UIImage(named: "Square-SampleImage1")!
    static let squareSampleImage2 = UIImage(named: "Square-SampleImage2")!
}
