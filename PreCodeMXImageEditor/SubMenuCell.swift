//
//  SubMenuCell.swift
//  PreCodeMXImageEditor
//
//  Created by Shubham Singh on 02/05/23.
//

import UIKit

class SubMenuCell: UICollectionViewCell {
    
    private let stackView: UIStackView = {
        let o = UIStackView()
        o.axis = .vertical
        //o.distribution = .fillEqually
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    private let nameLabelContainerView: UIView = {
        let o = UIView()
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    private let nameLabel: UILabel = {
        let o = UILabel()
        o.backgroundColor = .yellow
        o.font = UIFont.systemFont(ofSize: 12)
        o.textAlignment = .center
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    private let iconImageViewContainerView: UIView = {
        let o = UIView()
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    private let iconImageView: UIImageView = {
        let o = UIImageView()
        o.backgroundColor = .orange
        o.contentMode = .scaleAspectFit
        o.translatesAutoresizingMaskIntoConstraints = false
        return o
    }()
    
    override init(frame: CGRect) {
        super.init(frame: CGRect.zero)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    private func setupViews(for text: String? = nil, icon: UIImage? = nil) {
        if let text = text , let icon = icon {
            nameLabel.text = text
            iconImageView.image = icon
            setupIconAndText()
        }
        else if let text = text {
            nameLabel.text = text
            iconImageView.image = nil
            setupLabel()
        }
        else if let icon = icon {
            nameLabel.text = nil
            iconImageView.image = icon
            setupIcon()
        }
        else {
            nameLabel.text = nil
            iconImageView.image = nil
        }
    }
    
    func configure(with text: String, icon: UIImage) {
        setupViews(for: text, icon: icon)
    }
    
    func configure(with text: String) {
        setupViews(for: text)
    }
    
    func configure(with icon: UIImage) {
        setupViews(icon: icon)
    }
    
    private let spacing: CGFloat = 8
    
    
    private func setupIconAndText() {
        self.subviews.forEach { subview in subview.removeFromSuperview() }
        
        self.contentView.addSubview(self.stackView)
        self.contentView.addConstraints([
            NSLayoutConstraint(item: self.stackView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.stackView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.stackView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.stackView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: 0),
        ])
        
        self.stackView.addArrangedSubview(self.iconImageViewContainerView)
        self.stackView.addConstraint(NSLayoutConstraint(item: self.iconImageViewContainerView, attribute: .height, relatedBy: .equal, toItem: self.stackView, attribute: .height, multiplier: 0.7, constant: 0))
        
        self.iconImageViewContainerView.addSubview(self.iconImageView)
        self.iconImageViewContainerView.addConstraints([
            NSLayoutConstraint(item: self.iconImageView, attribute: .top, relatedBy: .equal, toItem: self.iconImageViewContainerView, attribute: .top, multiplier: 1, constant: spacing/2),
            NSLayoutConstraint(item: self.iconImageView, attribute: .bottom, relatedBy: .equal, toItem: self.iconImageViewContainerView, attribute: .bottom, multiplier: 1, constant: -spacing/2),
            NSLayoutConstraint(item: self.iconImageView, attribute: .leading, relatedBy: .equal, toItem: self.iconImageViewContainerView, attribute: .leading, multiplier: 1, constant: spacing),
            NSLayoutConstraint(item: self.iconImageView, attribute: .trailing, relatedBy: .equal, toItem: self.iconImageViewContainerView, attribute: .trailing, multiplier: 1, constant: -spacing),
        ])
        
        self.stackView.addArrangedSubview(self.nameLabelContainerView)
        self.stackView.addConstraint(NSLayoutConstraint(item: self.nameLabelContainerView, attribute: .height, relatedBy: .equal, toItem: self.stackView, attribute: .height, multiplier: 0.3, constant: 0))
        
        self.nameLabelContainerView.addSubview(self.nameLabel)
        self.nameLabelContainerView.addConstraints([
            NSLayoutConstraint(item: self.nameLabel, attribute: .top, relatedBy: .equal, toItem: self.nameLabelContainerView, attribute: .top, multiplier: 1, constant: 0),
            NSLayoutConstraint(item: self.nameLabel, attribute: .bottom, relatedBy: .equal, toItem: self.nameLabelContainerView, attribute: .bottom, multiplier: 1, constant: -spacing/2),
            NSLayoutConstraint(item: self.nameLabel, attribute: .leading, relatedBy: .equal, toItem: self.nameLabelContainerView, attribute: .leading, multiplier: 1, constant: spacing/2),
            NSLayoutConstraint(item: self.nameLabel, attribute: .trailing, relatedBy: .equal, toItem: self.nameLabelContainerView, attribute: .trailing, multiplier: 1, constant: -spacing/2),
        ])
    }
    
    private func setupIcon() {
        self.subviews.forEach { subview in subview.removeFromSuperview() }
        
        self.contentView.addSubview(self.iconImageView)
        self.contentView.addConstraints([
            NSLayoutConstraint(item: self.iconImageView, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: spacing),
            NSLayoutConstraint(item: self.iconImageView, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -spacing),
            NSLayoutConstraint(item: self.iconImageView, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: spacing),
            NSLayoutConstraint(item: self.iconImageView, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -spacing),
        ])
    }
    
    private func setupLabel() {
        self.subviews.forEach { subview in subview.removeFromSuperview() }
        
        self.contentView.addSubview(self.nameLabel)
        self.contentView.addConstraints([
            NSLayoutConstraint(item: self.nameLabel, attribute: .top, relatedBy: .equal, toItem: self.contentView, attribute: .top, multiplier: 1, constant: spacing),
            NSLayoutConstraint(item: self.nameLabel, attribute: .bottom, relatedBy: .equal, toItem: self.contentView, attribute: .bottom, multiplier: 1, constant: -spacing),
            NSLayoutConstraint(item: self.nameLabel, attribute: .leading, relatedBy: .equal, toItem: self.contentView, attribute: .leading, multiplier: 1, constant: spacing),
            NSLayoutConstraint(item: self.nameLabel, attribute: .trailing, relatedBy: .equal, toItem: self.contentView, attribute: .trailing, multiplier: 1, constant: -spacing),
        ])
    }
}
